require 'rails_helper'

RSpec.describe Api::V1::TagsController, type: :controller do
  let(:tag) { TagsService.create( {"attributes": {"title": "test tag"}}).response }

  describe "GET index" do
    before do
      tag
    end

    it "retun tags" do
      get :index
      expect(JSON.parse(response.body)["data"].length).to eq(1)
    end
  end

  describe "POST create" do
    it "success" do
      title = "test tag"
      post :create, params: {"data": {"attributes": {"title": title}}}
      expect(parse_json(response.body)["data"]["attributes"]["title"]).to eq(title)
    end
  end

  describe "PATCH Update" do
    
    it "not found" do
      patch :update, params: {id: 999999}
      expect(response.status).to eq(404)
    end

    it "success" do
      new_title = "Updated Task Title"
      update_params  = {
        id: tag.id, #test specific
        "data":
      	{	"type": "tags",
      		"id": tag.id,

      		"attributes":{
      			"title": new_title
      		}
      	}
      }

      patch :update, params: update_params
      expect(parse_json(response.body)["data"]["attributes"]["title"]).to eq(new_title)
    end
  end
end
