require 'rails_helper'

RSpec.describe Api::V1::TasksController, type: :controller do
  let(:task) { TasksService.create( {"attributes": {"title": "test task"}}).response }

  describe "GET index" do
    before do
      task
    end

    it "retun tasks" do
      get :index
      expect(JSON.parse(response.body)["data"].length).to eq(1)
    end
  end

  describe "POST create" do
    it "success" do
      title = "test task"
      post :create, params: {"data": {"attributes": {"title": title}}}
      expect(parse_json(response.body)["data"]["attributes"]["title"]).to eq(title)
    end
  end

  describe "DELETE Task" do
    it "success" do
      delete :destroy, params: {id: task.id}
      expect(parse_json(response.body)["data"]["id"].to_i).to eq(task.id)
    end

    it "not found" do
      delete :destroy, params: {id: 9999}
      expect(response.status).to eq(404)
    end
  end

  describe "PATCH Update" do
    
    it "not found" do
      patch :update, params: {id: 999999}
      expect(response.status).to eq(404)
    end
    
    it "update task" do
      new_title = "Updated Task Title"
      update_params  = {
        id: task.id, #test specific
        "data":
      	{	"type": "tasks",
      		"id": task.id,

      		"attributes":{
      			"title": new_title
      		}
      	}
      }

      patch :update, params: update_params
      expect(parse_json(response.body)["data"]["attributes"]["title"]).to eq(new_title)
    end

    it "update task with tags" do
      update_params  = {
        id: task.id, #test specific
        "data":
      	{	"type": "tasks",
      		"id": task.id,

      		"attributes": {
      			"title": "Updated Task Title",
      			"tags": ["Urgent", "Home"]
      		}
      	}
      }
      update_params2  = {
        id: task.id, #test specific
        "data":
      	{	"type": "tasks",
      		"id": task.id,

      		"attributes": {
      			"title": "Updated Task Title",
      			"tags": ["Urgent", "Home2"]
      		}
      	}
      }

      patch :update, params: update_params
      patch :update, params: update_params2
      expect(parse_json(response.body)["data"]["relationships"]["tags"]["data"].length).to eq(3)
    end
  end
end
