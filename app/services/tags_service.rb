class TagsService

  class << self
    def create(attrs)
      tag = Tag.create(attrs)
      if tag.valid?
        ServiceResponse.new(:ok, tag)
      else
        ServiceResponse.new(:bad_request, tag.errors.messages)
      end
    end

    def find_and_update(attrs)
      tag_resp = find(attrs[:id])
      if tag_resp.status == :ok
        update(tag_resp.response, attrs)
      else
        tag_resp
      end
    end

    def update(tag, attrs)
      tag.update(attrs)
      if tag.valid?
        ServiceResponse.new(:ok, tag)
      else
        ServiceResponse.new(:error, tag.errors.messages)
      end
    end

    def find_by_names(tag_names)
      Tag.where(title: tag_names)
    end

    def find(id)
      tag = Tag.find_by({id: id})
      if tag
        ServiceResponse.new(:ok, tag)
      else
        ServiceResponse.new(:not_found, nil)
      end
    end

    def getAllTags()
      ServiceResponse.new(:ok, Tag.all())
    end
  end
end
