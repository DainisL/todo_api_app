class TasksService

  class << self
    def create(attrs)
      task = Task.create(attrs)
      if task.valid?
        ServiceResponse.new(:ok, task)
      else
        ServiceResponse.new(:bad_request, task.errors.messages)
      end
    end

    def delete(id)
      task_resp = find(id)
      if task_resp.status == :ok
        task_resp.response.destroy
        ServiceResponse.new(:ok, task_resp.response)
      else
        task_resp
      end
    end

    def find_and_update(attrs)
      task_resp = find(attrs[:id])
      if task_resp.status == :ok
        update(task_resp.response, attrs)
      else
        task_resp
      end
    end

    def update(task, attrs)

      if !!attrs[:tags]
        assign_tags(task, attrs[:tags])
        attrs.delete(:tags)
      end

      task.update(attrs)
      if task.valid?
        ServiceResponse.new(:ok, task)
      else
        ServiceResponse.new(:error, task.errors.messages)
      end
    end

    def assign_tags(task, tag_names)
      tag_diff =  tag_names - task.tags.map {|t| t.title}

      tag_diff.each do |tag|
        task.tags << Tag.where("title": tag).first_or_create
      end
      task
    end

    def find(id)
      task = Task.find_by({id: id})
      
      if task
        ServiceResponse.new(:ok, task)
      else
        ServiceResponse.new(:not_found, nil)
      end
    end

    def getAllTasks()
      ServiceResponse.new(:ok, Task.all())
    end
  end
end
