module Api
  module V1
    class TagsController < ApplicationController
      def index
        service_response = TagsService.getAllTags()
        render json: service_response.response, status: service_response.status
      end

      def create
        service_response = TagsService.create(create_params)
        render json: service_response.response, status: service_response.status
      end

      def update
        service_response = TagsService.find_and_update(update_params)
        render json: service_response.response, status: service_response.status
      end

    private
      def create_params
        ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title])
      end

      def update_params
        ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:id, :title])
      end
    end
  end
end
