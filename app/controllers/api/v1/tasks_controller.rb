module Api
  module V1
    class TasksController < ApplicationController
      def index
        service_response = TasksService.getAllTasks()
        render json: service_response.response, status: service_response.status
      end

      def create
        service_response = TasksService.create(create_params)
        render json: service_response.response, status: service_response.status
      end

      def update
        service_response = TasksService.find_and_update(update_params)
        render json: service_response.response, status: service_response.status
      end

      def destroy
        service_response = TasksService.delete(params[:id])
        render json: service_response.response, status: service_response.status
      end

    private
      def create_params
        ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title])
      end

      def update_params
        ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:id, :title, :tags])
      end
    end
  end
end
