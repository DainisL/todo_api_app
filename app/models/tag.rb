class Tag < ApplicationRecord
  validates :title, presence: true, uniqueness: true
  has_many :task_tags
  has_many :tasks, through: :task_tags
end
